const path = require('path');
const webpack = require('webpack');
const MergeIntoSingleFilePlugin = require('webpack-merge-and-include-globally');

module.exports = {
  entry: './src/index.js',  
  output: {
    filename: '[name]',
    path: path.resolve(__dirname, 'web/static'),
  },
  plugins: [
    new MergeIntoSingleFilePlugin({
        files: {
            "vendor.js": [
                'node_modules/jquery/dist/jquery.min.js',
                'node_modules/datatables.net/js/dataTables.min.js',
                'node_modules/datatables.net-dt/js/dataTables.dataTables.js'             
            ],
            "vendor.css": [
                'node_modules/datatables.net-dt/css/dataTables.dataTables.min.css'
            ],
            "main.js": [
              './src/util.js',
              './src/util2.js'                           
          ],
        }
    }),
]
};